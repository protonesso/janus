# Maintainer: protonesso <nagakamira@gmail.com>

pkgname=tor
pkgver=0.3.3.6
pkgrel=1
pkgdesc='Anonymizing overlay network.'
arch=('x86_64' 'aarch64')
url='https://www.torproject.org/'
license=('BSD')
groups=('base')
depends=('musl' 'zlib' 'libressl' 'libevent')
backup=('etc/tor/torrc'
	'etc/tor/torrc-dist')
source=("https://www.torproject.org/dist/$pkgname-$pkgver.tar.gz"
	'tor'
	'tor-log')
install=$pkgname.install

build() {
	cd "$srcdir/$pkgname-$pkgver"
	./configure \
		--prefix=/usr \
		--sysconfdir=/etc \
		--localstatedir=/var \
		--build=$XHOST \
		--host=$XTARGET \
		--disable-seccomp
	make
}

package() {
	unset MAKEFLAGS
	cd "$srcdir/$pkgname-$pkgver"
	make DESTDIR=$pkgdir install

	install -Dm0755 $srcdir/tor $pkgdir/etc/service/tor/run
	install -Dm0755 $srcdir/tor-log $pkgdir/etc/service/tor/log/run

	mkdir -p $pkgdir/var/log/tor $pkgdir/run/tor

	mv $pkgdir/etc/tor/torrc.sample $pkgdir/etc/tor/torrc

	msg "Stripping package $pkgname..."
	find $pkgdir -type f | xargs file 2>/dev/null | grep "LSB executable"       | cut -f 1 -d : | xargs $STRIP --strip-all			2>/dev/null || true
	find $pkgdir -type f | xargs file 2>/dev/null | grep "shared object"        | cut -f 1 -d : | xargs $STRIP --strip-unneeded		2>/dev/null || true
	find $pkgdir -type f | xargs file 2>/dev/null | grep "current ar archive"   | cut -f 1 -d : | xargs $STRIP --strip-debug		2>/dev/null || true
	cd $pkgdir
	rm -rf usr/{,local/}{,share/}{doc,man,info,locale} usr{,/local}{,/share},opt/*}/{man,info} usr/{,local/}{,share/}{doc,gtk-doc} opt/*/{doc,gtk-doc}
	rm -rf $pkgdir/{,usr/}lib/charset.alias
}
