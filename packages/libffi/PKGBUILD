# Maintainer: protonesso <nagakamira@gmail.com>

pkgname=libffi
pkgver=3.2.1
pkgrel=1
pkgdesc="Portable foreign function interface library"
arch=('x86_64' 'aarch64')
url="http://sourceware.org/libffi/"
license=('MIT')
groups=('base')
depends=('musl')
source=("ftp://sourceware.org/pub/$pkgname/$pkgname-$pkgver.tar.gz")

prepare() {
	cd "$srcdir/$pkgname-$pkgver"
	sed -e '/^includesdir/ s/$(libdir).*$/$(includedir)/' \
		-i include/Makefile.in
	sed -e '/^includedir/ s/=.*$/=@includedir@/' \
		-e 's/^Cflags: -I${includedir}/Cflags:/' \
		-i libffi.pc.in
}

build() {
	cd "$srcdir/$pkgname-$pkgver"
	./configure \
		--prefix=/usr \
		--build=$XHOST \
		--host=$XTARGET
	make
}

package() {
	unset MAKEFLAGS
	cd "$srcdir/$pkgname-$pkgver"
	make DESTDIR=$pkgdir install

	msg "Stripping package $pkgname..."
	find $pkgdir -type f | xargs file 2>/dev/null | grep "LSB executable"       | cut -f 1 -d : | xargs $STRIP --strip-all			2>/dev/null || true
	find $pkgdir -type f | xargs file 2>/dev/null | grep "shared object"        | cut -f 1 -d : | xargs $STRIP --strip-unneeded		2>/dev/null || true
	find $pkgdir -type f | xargs file 2>/dev/null | grep "current ar archive"   | cut -f 1 -d : | xargs $STRIP --strip-debug		2>/dev/null || true
	cd $pkgdir
	rm -rf usr/{,local/}{,share/}{doc,man,info,locale} usr{,/local}{,/share},opt/*}/{man,info} usr/{,local/}{,share/}{doc,gtk-doc} opt/*/{doc,gtk-doc}
	rm -rf $pkgdir/{,usr/}lib/charset.alias
}
