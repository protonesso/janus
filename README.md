# Welcome to januslinux - Fast and mini Linux distribution which uses musl libc.

## What is this?

januslinux is fast and mini [Linux](https://www.kernel.org/) distribution which uses [musl libc](http://www.musl-libc.org/). It is designed for maximum performance and small size. It is including basic system programs, development tools, web browser and ssh client(will be added more!). januslinux uses [LibreSSL](https://www.libressl.org/) as SSL engine because it is small and clean.

## For who?

januslinux was made for people who like CLI interface and to get Linux knowledge. It allows call januslinux: distribution for beginners.

## What hardware I can run it?

You can run it on x86_64, aarch64.

## Where I can get it?

You can download [current beta version](https://github.com/JanusLinux/janus/releases/download/1.0-beta/januslinux-1.0-beta.iso) of januslinux which compiled on 25th April of 2018. It have bugs.

## How I can contact and help you with development?

You can write me on [email](mailto:nagakamira@gmail.com) and give some suggestions or help me on [GitHub](https://github.com/JanusLinux/janus). Also you can help submit it on DistroWatch, click [here](http://distrowatch.org/dwres.php?waitingdistro=444&resource=links#new). We are have own Discord server, click [here](https://discord.gg/a329dDz).

## Releases information and roadmap

Roadmap and information of januslinux are available [here](https://januslinux.github.io/releases/).
